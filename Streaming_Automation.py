from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.support import expected_conditions as EC


class Streaming:
    def __init__(self):
        self.browser = webdriver.Chrome(executable_path=ChromeDriverManager().install())
        self.wait = WebDriverWait(self.browser, 20)
        self.browser.maximize_window()

    def start_streaming(self, url):
        self.browser.get(url)
        button = '#register'
        self.wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR, button)))
        self.browser.find_element_by_css_selector(button).click()

    def stop_streaming(self):
        self.browser.close()

