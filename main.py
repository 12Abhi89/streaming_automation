import Streaming_Automation
import time

if __name__ == "__main__":
    stream = Streaming_Automation.Streaming()
    url = "https://media.evmlabs.com/*.html"
    stream.start_streaming(url)
    time.sleep(10)
    stream.stop_streaming()
